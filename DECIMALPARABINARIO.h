#ifndef DEDECIMALPARABINARIO_H_INCLUDED
#define DEDECIMALPARABINARIO_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
int converteforbinario(int n)
{
    setlocale(LC_ALL, "Portuguese");
    printf("Digite um n�mero: ");
    scanf("%d", &n);

    int i = 0, j, vetBi[50];

	while(n > 0)
	{
		vetBi[i] = n % 2;
		i++;
		n = n / 2;
	}

	printf("Binário: ");

	for(j = i - 1; j >= 0; j--)
    {
        printf("%d", vetBi[j]);
    }
	printf("\n");

	return 0;
}
int main(void)
{
    int n;
    converteforbinario(n);
}
#endif // DEDECIMALPARABINARIO_H_INCLUDED
