#ifndef DECIMALPARAOCTAL_H_INCLUDED
#define DECIMALPARAOCTAL_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

int decimal_octal(int decimal){
    int octal = 0;
    int resto, i = 1;
    printf("valor decimal: ");
    scanf("%d", &decimal);
    while(decimal != 0) {
        resto = decimal % 8;
        decimal = decimal / 8;
        octal = octal + (resto * i);
        i = i * 10;
    }printf("Octal: %d ", octal);

    printf("\n\n");
    system("PAUSE");

    return octal;
}

    int main(void){
    int decimal;


    decimal_octal(decimal);


    return 0;
}

#endif // DECIMALPARAOCTAL_H_INCLUDED
